/*
 Navicat Premium Data Transfer

 Source Server         : 5.7
 Source Server Type    : MySQL
 Source Server Version : 50722
 Source Host           : localhost:3306
 Source Schema         : bond

 Target Server Type    : MySQL
 Target Server Version : 50722
 File Encoding         : 65001

 Date: 26/07/2020 23:14:37
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for bond_dept
-- ----------------------------
DROP TABLE IF EXISTS `bond_dept`;
CREATE TABLE `bond_dept`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '名称',
  `pid` int(11) DEFAULT NULL COMMENT '父级id',
  `orders` int(11) DEFAULT NULL COMMENT '序号',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '描述',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bond_dept
-- ----------------------------
INSERT INTO `bond_dept` VALUES (2, '23422', -1, 2, '2342', '2020-07-11 12:50:51', NULL);
INSERT INTO `bond_dept` VALUES (3, '4324', 2, 23, '234234', '2020-07-11 12:51:02', NULL);

-- ----------------------------
-- Table structure for bond_line
-- ----------------------------
DROP TABLE IF EXISTS `bond_line`;
CREATE TABLE `bond_line`  (
  `start_point` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `end_point` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`start_point`, `end_point`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bond_line
-- ----------------------------
INSERT INTO `bond_line` VALUES ('helloword', 'auth-manager-server');
INSERT INTO `bond_line` VALUES ('helloword', 'auth-server');

-- ----------------------------
-- Table structure for bond_oauth_client
-- ----------------------------
DROP TABLE IF EXISTS `bond_oauth_client`;
CREATE TABLE `bond_oauth_client`  (
  `client_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '终端编号',
  `client_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `resource_ids` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '资源ID标识',
  `client_secret` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '终端安全码',
  `scope` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '终端授权范围',
  `authorized_grant_types` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '终端授权类型',
  `web_server_redirect_uri` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '服务器回调地址',
  `authorities` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '访问资源所需权限',
  `access_token_validity` int(11) DEFAULT NULL COMMENT '设定终端的access_token的有效时间值（秒）',
  `refresh_token_validity` int(11) DEFAULT NULL COMMENT '设定终端的refresh_token的有效时间值（秒）',
  `additional_information` varchar(4096) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '附加信息',
  `autoapprove` tinyint(4) DEFAULT NULL COMMENT '是否登录时跳过授权',
  PRIMARY KEY (`client_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '终端配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bond_oauth_client
-- ----------------------------
INSERT INTO `bond_oauth_client` VALUES ('auth-manager-server', '权限中心', '', '$2a$10$y2hKeELx.z3Sbz.kjQ4wmuiIsv5ZSbUQ1ov4BwFH6ccirP8Knp1uq', 'server', 'password,client_credentials,refresh_token', '', NULL, 3600, 7200, NULL, NULL);
INSERT INTO `bond_oauth_client` VALUES ('helloword', '测试项目', '', '$2a$10$y2hKeELx.z3Sbz.kjQ4wmuiIsv5ZSbUQ1ov4BwFH6ccirP8Knp1uq', 'server', 'password,refresh_token,client_credentials', '', NULL, 3600, 7200, NULL, NULL);

-- ----------------------------
-- Table structure for bond_permission
-- ----------------------------
DROP TABLE IF EXISTS `bond_permission`;
CREATE TABLE `bond_permission`  (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'api地址',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '名称',
  `client_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '客户端id',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uni_url_clientid`(`url`, `client_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1460 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '权限信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bond_permission
-- ----------------------------
INSERT INTO `bond_permission` VALUES (7, '/helloword/hello', 'hello', 'helloword');
INSERT INTO `bond_permission` VALUES (17, '/error', 'errorHtml', 'auth-manager-server');
INSERT INTO `bond_permission` VALUES (18, '/userInfo/saveUserRole', 'saveUserRoles', 'auth-manager-server');
INSERT INTO `bond_permission` VALUES (19, '/userInfo/delete', 'delete', 'auth-manager-server');
INSERT INTO `bond_permission` VALUES (20, '/client/list', 'list', 'auth-manager-server');
INSERT INTO `bond_permission` VALUES (21, '/userInfo/viewMenu', 'viewMenu', 'auth-manager-server');
INSERT INTO `bond_permission` VALUES (22, '/role/delete/{roleCode}', 'delete', 'auth-manager-server');
INSERT INTO `bond_permission` VALUES (23, '/userInfo/list', 'list', 'auth-manager-server');
INSERT INTO `bond_permission` VALUES (24, '/sys/queryPermissions', 'queryPermissions', 'auth-manager-server');
INSERT INTO `bond_permission` VALUES (25, '/userInfo/checkUsernameUnique', 'checkUsernameUnique', 'auth-manager-server');
INSERT INTO `bond_permission` VALUES (26, '/router/savePermission', 'savePermission', 'auth-manager-server');
INSERT INTO `bond_permission` VALUES (27, '/sys/info', 'getInfo', 'auth-manager-server');
INSERT INTO `bond_permission` VALUES (28, '/sys/checkClient', 'checkClient', 'auth-manager-server');
INSERT INTO `bond_permission` VALUES (29, '/role/list', 'list', 'auth-manager-server');
INSERT INTO `bond_permission` VALUES (30, '/sys/relationship', 'relationship', 'auth-manager-server');
INSERT INTO `bond_permission` VALUES (31, '/router/list', 'list', 'auth-manager-server');
INSERT INTO `bond_permission` VALUES (32, '/dept/list', 'list', 'auth-manager-server');
INSERT INTO `bond_permission` VALUES (33, '/sys/registerRelationship', 'registerRelationship', 'auth-manager-server');
INSERT INTO `bond_permission` VALUES (34, '/router/deletePermission', 'deletePermission', 'auth-manager-server');
INSERT INTO `bond_permission` VALUES (35, '/userInfo/save', 'save', 'auth-manager-server');
INSERT INTO `bond_permission` VALUES (36, '/role/listPerAndRouter/{roleCode}', 'listPerAndRouter', 'auth-manager-server');
INSERT INTO `bond_permission` VALUES (37, '/userInfo/loadUserDept', 'loadUserDept', 'auth-manager-server');
INSERT INTO `bond_permission` VALUES (38, '/role/all', 'listAll', 'auth-manager-server');
INSERT INTO `bond_permission` VALUES (39, '/router/update', 'update', 'auth-manager-server');
INSERT INTO `bond_permission` VALUES (40, '/dept/delete/{id}', 'delete', 'auth-manager-server');
INSERT INTO `bond_permission` VALUES (41, '/router/save', 'save', 'auth-manager-server');
INSERT INTO `bond_permission` VALUES (42, '/router/delete', 'delete', 'auth-manager-server');
INSERT INTO `bond_permission` VALUES (43, '/userInfo/listUserRoles', 'listUserRoles', 'auth-manager-server');
INSERT INTO `bond_permission` VALUES (44, '/role/save', 'saveObject', 'auth-manager-server');
INSERT INTO `bond_permission` VALUES (45, '/dept/save', 'save', 'auth-manager-server');
INSERT INTO `bond_permission` VALUES (205, '/permission/list', 'list', 'auth-manager-server');
INSERT INTO `bond_permission` VALUES (365, '/permission/listPage', 'listPage', 'auth-manager-server');
INSERT INTO `bond_permission` VALUES (431, '/permission/delete/{id}', 'delete', 'auth-manager-server');
INSERT INTO `bond_permission` VALUES (465, '/sys/reloadPermission', 'reload', 'auth-manager-server');
INSERT INTO `bond_permission` VALUES (499, '/permission/saveclients', 'saveclients', 'auth-manager-server');
INSERT INTO `bond_permission` VALUES (1409, '/error', 'errorHtml', 'helloword');
INSERT INTO `bond_permission` VALUES (1410, '/sys/reloadPermission', 'reload', 'helloword');

-- ----------------------------
-- Table structure for bond_permission_client
-- ----------------------------
DROP TABLE IF EXISTS `bond_permission_client`;
CREATE TABLE `bond_permission_client`  (
  `client_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `permission_id` bigint(11) NOT NULL,
  PRIMARY KEY (`client_id`, `permission_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bond_permission_client
-- ----------------------------
INSERT INTO `bond_permission_client` VALUES ('auth-manager-server', 7);
INSERT INTO `bond_permission_client` VALUES ('auth-manager-server', 17);
INSERT INTO `bond_permission_client` VALUES ('auth-manager-server', 18);
INSERT INTO `bond_permission_client` VALUES ('auth-manager-server', 19);
INSERT INTO `bond_permission_client` VALUES ('auth-manager-server', 20);
INSERT INTO `bond_permission_client` VALUES ('auth-manager-server', 21);
INSERT INTO `bond_permission_client` VALUES ('auth-manager-server', 23);
INSERT INTO `bond_permission_client` VALUES ('auth-manager-server', 24);
INSERT INTO `bond_permission_client` VALUES ('auth-manager-server', 25);
INSERT INTO `bond_permission_client` VALUES ('auth-manager-server', 33);
INSERT INTO `bond_permission_client` VALUES ('auth-manager-server', 365);
INSERT INTO `bond_permission_client` VALUES ('helloword', 7);
INSERT INTO `bond_permission_client` VALUES ('helloword', 17);
INSERT INTO `bond_permission_client` VALUES ('helloword', 18);
INSERT INTO `bond_permission_client` VALUES ('helloword', 19);
INSERT INTO `bond_permission_client` VALUES ('helloword', 20);
INSERT INTO `bond_permission_client` VALUES ('helloword', 21);
INSERT INTO `bond_permission_client` VALUES ('helloword', 23);
INSERT INTO `bond_permission_client` VALUES ('helloword', 25);
INSERT INTO `bond_permission_client` VALUES ('helloword', 365);

-- ----------------------------
-- Table structure for bond_role_info
-- ----------------------------
DROP TABLE IF EXISTS `bond_role_info`;
CREATE TABLE `bond_role_info`  (
  `ROLE_CODE` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '角色编码',
  `ROLE_NAME` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '角色名称',
  `ROLE_TYPE` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'F 为系统 固有的 G 全局的 P 公用的 D 部门的 I 为项目角色 W工作量角色',
  `UNIT_CODE` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '角色类型',
  `IS_VALID` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ROLE_DESC` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '是否启用',
  `update_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `creator` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '创建者',
  `updator` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`ROLE_CODE`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bond_role_info
-- ----------------------------
INSERT INTO `bond_role_info` VALUES ('ADMIN', '管理员', '1', '1', 'T', '1', '2020-07-25 00:47:00', NULL, '', 'admin');

-- ----------------------------
-- Table structure for bond_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `bond_role_permission`;
CREATE TABLE `bond_role_permission`  (
  `role_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '角色编码',
  `permission_id` int(11) NOT NULL COMMENT '权限id',
  PRIMARY KEY (`role_code`, `permission_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bond_role_permission
-- ----------------------------
INSERT INTO `bond_role_permission` VALUES ('ADMIN', 4);
INSERT INTO `bond_role_permission` VALUES ('ADMIN', 20);

-- ----------------------------
-- Table structure for bond_role_router
-- ----------------------------
DROP TABLE IF EXISTS `bond_role_router`;
CREATE TABLE `bond_role_router`  (
  `router_id` int(11) NOT NULL COMMENT '路由id',
  `role_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '角色编码',
  `client_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '客户端id',
  PRIMARY KEY (`router_id`, `role_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bond_role_router
-- ----------------------------
INSERT INTO `bond_role_router` VALUES (1, 'ADMIN', 'auth-manager-server');
INSERT INTO `bond_role_router` VALUES (2, 'ADMIN', 'auth-manager-server');
INSERT INTO `bond_role_router` VALUES (3, 'ADMIN', 'auth-manager-server');
INSERT INTO `bond_role_router` VALUES (4, 'ADMIN', 'auth-manager-server');
INSERT INTO `bond_role_router` VALUES (6, 'ADMIN', 'auth-manager-server');
INSERT INTO `bond_role_router` VALUES (7, 'ADMIN', 'auth-manager-server');
INSERT INTO `bond_role_router` VALUES (8, 'ADMIN', 'auth-manager-server');
INSERT INTO `bond_role_router` VALUES (9, 'ADMIN', 'auth-manager-server');

-- ----------------------------
-- Table structure for bond_router_info
-- ----------------------------
DROP TABLE IF EXISTS `bond_router_info`;
CREATE TABLE `bond_router_info`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '名称',
  `parentId` bigint(20) DEFAULT NULL COMMENT '父级id',
  `orders` int(11) DEFAULT NULL COMMENT '序号',
  `path` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '路由地址',
  `client_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '客户端id',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '展示名称',
  `component` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '路由组件',
  `redirect` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '重定向地址',
  `icon` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '图标',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bond_router_info
-- ----------------------------
INSERT INTO `bond_router_info` VALUES (1, '菜单管理', -1, 1, '/menu', 'auth-manager-server', '菜单管理', 'upms/menu/index', NULL, 'tree');
INSERT INTO `bond_router_info` VALUES (2, '角色管理', -1, 2, '/role', 'auth-manager-server', '角色管理', 'upms/role/index', NULL, 'role');
INSERT INTO `bond_router_info` VALUES (3, '用户管理', -1, 3, '/user', 'auth-manager-server', '用户管理', 'upms/userInfo/index', NULL, 'user');
INSERT INTO `bond_router_info` VALUES (4, '技术栈', -1, 4, '/dependence', 'auth-manager-server', '技术栈', 'upms/dependence', NULL, 'jishu');
INSERT INTO `bond_router_info` VALUES (6, '数据字典', -1, 6, '/dictionary', 'auth-manager-server', '数据字典', 'upms/dictionary/index', NULL, 'nested');
INSERT INTO `bond_router_info` VALUES (7, '部门管理', -1, 7, '/dept', 'auth-manager-server', '部门管理', 'upms/dept/index', NULL, 'user');
INSERT INTO `bond_router_info` VALUES (8, 'bond', -1, 1, '/bond', 'auth-manager-server', 'bond', 'upms/bond/index', NULL, 'eye');
INSERT INTO `bond_router_info` VALUES (9, '权限列表', -1, 4, '/permission', 'auth-manager-server', '权限列表', 'upms/permission/index', NULL, 'jishu');

-- ----------------------------
-- Table structure for bond_router_permission
-- ----------------------------
DROP TABLE IF EXISTS `bond_router_permission`;
CREATE TABLE `bond_router_permission`  (
  `router_id` bigint(11) NOT NULL,
  `permission_id` bigint(11) NOT NULL,
  PRIMARY KEY (`router_id`, `permission_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bond_router_permission
-- ----------------------------
INSERT INTO `bond_router_permission` VALUES (1, 4);
INSERT INTO `bond_router_permission` VALUES (8, 4);
INSERT INTO `bond_router_permission` VALUES (8, 18);
INSERT INTO `bond_router_permission` VALUES (8, 20);
INSERT INTO `bond_router_permission` VALUES (8, 21);

-- ----------------------------
-- Table structure for bond_user_dept
-- ----------------------------
DROP TABLE IF EXISTS `bond_user_dept`;
CREATE TABLE `bond_user_dept`  (
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `dept_id` int(11) NOT NULL COMMENT '部门id',
  `post_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '岗位id',
  PRIMARY KEY (`user_id`, `dept_id`, `post_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for bond_user_info
-- ----------------------------
DROP TABLE IF EXISTS `bond_user_info`;
CREATE TABLE `bond_user_info`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '姓名',
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '登陆名',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '密码',
  `introduction` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '介绍',
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '头像',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bond_user_info
-- ----------------------------
INSERT INTO `bond_user_info` VALUES (1, '管理员', 'admin', '$2a$10$Rm9cO1Ritej2T9zQ12pGx.5rD1urInYrPkkI6tf5g3/GXeo5e99si', 'a', 'a');

-- ----------------------------
-- Table structure for bond_user_role
-- ----------------------------
DROP TABLE IF EXISTS `bond_user_role`;
CREATE TABLE `bond_user_role`  (
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `role_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '角色编码'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bond_user_role
-- ----------------------------
INSERT INTO `bond_user_role` VALUES (1, 'ADMIN');

SET FOREIGN_KEY_CHECKS = 1;
