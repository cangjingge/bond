package com.chungkui.bond.oauth2.server.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chungkui.bond.authmanager.bean.UserInfo;
import com.chungkui.bond.oauth2.server.mapper.UserInfoMapper;
import com.chungkui.bond.oauth2.server.service.UserInfoService;
import org.springframework.stereotype.Service;

/**
 * Copyright (C), 2019/6/22, JASON
 * 〈用户管理〉<br>
 * 〈功能详细描述〉
 *
 * @author jason
 * @fileName: UserInfoServiceImpl.java
 * @date: 2019/6/22 12:02
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
@Service
public class UserInfoServiceImpl extends ServiceImpl<UserInfoMapper, UserInfo> implements UserInfoService {

    @Override
    public UserInfo getByUsername(String username) {
        QueryWrapper<UserInfo> userInfoWrapper = new QueryWrapper<UserInfo>();
        userInfoWrapper.eq("username", username);
        return super.getOne(userInfoWrapper);
    }

}
