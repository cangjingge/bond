package com.chungkui.bond.oauth2.server.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chungkui.bond.authmanager.bean.UserInfo;
import com.chungkui.bond.authmanager.bean.UserRole;
import com.chungkui.bond.oauth2.server.mapper.UserRoleInfoMapper;
import com.chungkui.bond.oauth2.server.service.UserRoleService;
import com.google.common.collect.Sets;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleInfoMapper, UserRole> implements UserRoleService {
    @Override
    public Set<GrantedAuthority> listRoleCodesByUserInfo(UserInfo userInfo) {
        Set<String> roles = baseMapper.listRoleCodesByUserInfo(userInfo);
        Set<GrantedAuthority> set = Sets.newHashSet();
        if (CollectionUtils.isNotEmpty(roles)) {
            for (String s : roles) {
                set.add(new SimpleGrantedAuthority(s));
            }
        }
        return set;
    }
}
