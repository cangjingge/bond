package com.chungkui.bond.oauth2.server.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.chungkui.bond.authmanager.bean.UserDept;
import com.chungkui.bond.authmanager.bean.UserInfo;

import java.util.List;

/**
 * Copyright (C), 2019/5/29, JASON
 * 〈用户信息serevice〉<br>
 * 〈功能详细描述〉
 *
 * @author jason
 * @fileName: UserInfoService.java
 * @date: 2019/5/29 20:49
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
public interface UserInfoService extends IService<UserInfo> {


    /**
     * @param username
     * @return
     */
    UserInfo getByUsername(String username);

}
