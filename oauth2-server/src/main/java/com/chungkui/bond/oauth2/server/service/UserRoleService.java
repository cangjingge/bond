package com.chungkui.bond.oauth2.server.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.chungkui.bond.authmanager.bean.UserInfo;
import com.chungkui.bond.authmanager.bean.UserRole;
import org.springframework.security.core.GrantedAuthority;

import java.util.Set;


/**
 * Copyright (C), 2019/5/29, JASON
 * 〈角色信息service〉<br>
 * 〈功能详细描述〉
 *
 * @author Jwy
 * @fileName: RoleInfoService.java
 * @date: 2019/5/29 20:49
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
public interface UserRoleService extends IService<UserRole> {

    /**
     * 根据用户信息获取角色列表
     *
     * @param user
     * @return
     */
    Set<GrantedAuthority>  listRoleCodesByUserInfo(UserInfo user);


}
