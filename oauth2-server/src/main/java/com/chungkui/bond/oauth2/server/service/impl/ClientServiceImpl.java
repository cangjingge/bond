package com.chungkui.bond.oauth2.server.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chungkui.bond.authmanager.bean.Client;


import com.chungkui.bond.oauth2.server.mapper.ClientMapper;
import com.chungkui.bond.oauth2.server.service.ClientService;
import org.springframework.stereotype.Service;


@Service
public class ClientServiceImpl extends ServiceImpl<ClientMapper, Client> implements ClientService {

}
