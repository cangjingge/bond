package com.chungkui.bond.oauth2.server.config;

import org.springframework.security.oauth2.provider.NoSuchClientException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class NoSuchClientExceptionHandler {
    @ResponseBody
    @ExceptionHandler(NoSuchClientException.class)
    public Map<String, Object> handleException(Exception e) {
        Map<String, Object> map = new HashMap<>();
        map.put("code", "nosuchclient");
        map.put("message", e.getMessage());
        return map;
    }
}
