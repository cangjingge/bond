package com.chungkui.bond.oauth2.server.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.NoSuchClientException;
import org.springframework.security.oauth2.provider.client.JdbcClientDetailsService;

import javax.sql.DataSource;


public class ClientDetailsServiceImpl extends JdbcClientDetailsService
{    private final static Logger LOG = LoggerFactory.getLogger(ClientDetailsServiceImpl.class);

    public ClientDetailsServiceImpl(DataSource dataSource)
    {
        super(dataSource);
        super.setSelectClientDetailsSql("select client_id, client_secret, resource_ids, scope,  authorized_grant_types," +
                " web_server_redirect_uri, authorities, access_token_validity, " +
                "refresh_token_validity, additional_information, autoapprove from bond_oauth_client  where client_id = ?");
        super.setFindClientDetailsSql("select client_id, client_secret, resource_ids, scope,  authorized_grant_types," +
                " web_server_redirect_uri, authorities, access_token_validity, " +
                "refresh_token_validity, additional_information, autoapprove from bond_oauth_client  order by client_id");
    }

    @Override
    public ClientDetails loadClientByClientId(String clientId)
    {
        try{
            return super.loadClientByClientId(clientId);
        }catch (Exception e){
            LOG.error("No client with requested id: " + clientId);
            throw new NoSuchClientException("No client with requested id: " + clientId);

        }

    }
}
