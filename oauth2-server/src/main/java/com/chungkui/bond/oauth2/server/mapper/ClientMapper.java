package com.chungkui.bond.oauth2.server.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chungkui.bond.authmanager.bean.Client;
import org.apache.ibatis.annotations.Mapper;

/**
 * 终端配置Mapper接口
 *
 * @author ruoyi
 */
@Mapper
public interface ClientMapper extends BaseMapper<Client>
{

}
