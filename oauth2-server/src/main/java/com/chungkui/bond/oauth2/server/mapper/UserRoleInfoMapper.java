package com.chungkui.bond.oauth2.server.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chungkui.bond.authmanager.bean.UserInfo;
import com.chungkui.bond.authmanager.bean.UserRole;

import org.apache.ibatis.annotations.Mapper;

import java.util.Set;

@Mapper
public interface UserRoleInfoMapper extends BaseMapper<UserRole> {
    Set<String> listRoleCodesByUserInfo(UserInfo userInfo);
}