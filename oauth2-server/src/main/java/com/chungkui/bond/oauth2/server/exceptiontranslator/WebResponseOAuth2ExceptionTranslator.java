package com.chungkui.bond.oauth2.server.exceptiontranslator;


import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.security.oauth2.provider.error.WebResponseExceptionTranslator;


public class WebResponseOAuth2ExceptionTranslator implements WebResponseExceptionTranslator<OAuth2Exception>
{
    @Override
    public ResponseEntity<OAuth2Exception> translate(Exception e)
    {
        return   ResponseEntity.status(200).body(new OAuth2Exception(e.getMessage()));
    }

}
