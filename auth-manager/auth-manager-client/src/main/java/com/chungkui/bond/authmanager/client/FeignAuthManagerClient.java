package com.chungkui.bond.authmanager.client;

import com.chungkui.bond.authmanager.bean.Client;
import com.chungkui.bond.authmanager.bean.Permission;
import com.chungkui.bond.common.core.api.ResBus;
import com.chungkui.bond.common.core.bean.BondNode;
import com.chungkui.bond.common.core.constant.MicroserviceConstants;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.List;

@Component
@FeignClient(contextId = "feignManagerClient", value = MicroserviceConstants.AUTH_MANAGER_SERVER)
public interface FeignAuthManagerClient extends AuthManagerClient{
     /**
     * 自动注册客户端到权限管理中心
     * 仅限开发环境使用生产环境仅管理员有权
     * @param client
     * @return
     */
    @Override
    @PostMapping("/sys/checkClient")
    ResBus<Boolean> checkClient(@RequestBody Client client);

    /**
     * 自动注册微服务拓扑关系到权限管理中心
     * 仅限开发环境使用生产环境仅管理员有权
     *
     * @param bondNode
     * @return
     */
    @Override
    @PostMapping("/sys/registerRelationship")
    ResBus<Boolean> registerRelationship(@RequestBody BondNode bondNode);

    @GetMapping("/sys/queryPermissions")
    ResBus<List<Permission>> queryPermissions(@RequestHeader("client_id") String client);
}
