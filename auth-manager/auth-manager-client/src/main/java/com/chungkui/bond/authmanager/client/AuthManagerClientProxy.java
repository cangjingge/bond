package com.chungkui.bond.authmanager.client;

import com.chungkui.bond.authmanager.bean.Client;
import com.chungkui.bond.authmanager.bean.Permission;
import com.chungkui.bond.common.core.api.ResBus;
import com.chungkui.bond.common.core.bean.BondNode;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.List;


public class AuthManagerClientProxy implements AuthManagerClient {

    AuthManagerClient authManagerClient;
    @Autowired(required = false)
    DbAuthManagerClient dbAuthManagerClient;
    @Autowired(required = false)
    FeignAuthManagerClient feignAuthManagerClient;

    @PostConstruct
    public void init() {
        if (dbAuthManagerClient != null) {
            authManagerClient = dbAuthManagerClient;
        } else {
            authManagerClient = feignAuthManagerClient;
        }
    }

    @Override
    public ResBus<Boolean> checkClient(Client client) {
        return authManagerClient.checkClient(client);
    }

    @Override
    public ResBus<Boolean> registerRelationship(BondNode bondNode) {
        return authManagerClient.registerRelationship(bondNode);
    }

    @Override
    public ResBus<List<Permission>> queryPermissions(String client) {
        return authManagerClient.queryPermissions(client);
    }
}
