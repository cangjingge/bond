package com.chungkui.bond.authmanager.client;

import com.chungkui.bond.authmanager.bean.Client;
import com.chungkui.bond.authmanager.bean.Permission;
import com.chungkui.bond.common.core.api.ResBus;
import com.chungkui.bond.common.core.bean.BondNode;

import java.util.List;


public interface AuthManagerClient {
    ResBus<Boolean> checkClient(Client client);

    ResBus<Boolean> registerRelationship(BondNode bondNode);

    ResBus<List<Permission>> queryPermissions(String client);
}
