package com.chungkui.bond.authmanager.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Set;

/**
 * Copyright (C), 2019/5/29, JASON
 * 〈权限bean〉<br>
 * 〈功能详细描述〉
 *
 * @author jason
 * @fileName: Permission.java
 * @date: 2019/5/29 20:14
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
@TableName("bond_permission")
public
class Permission implements  Serializable {
    @TableId(type = IdType.AUTO)
    String id;
    String url;
    String name;
    @TableField(exist = false)
    Boolean checked;
    @TableField(exist = false)
    Set<String> roles;
    @TableField(exist = false)
    Set<String> allowClients;
    String clientId;

    public Set<String> getRoles() {
        return roles;
    }


    public String getUrl() {
        return url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    public void setRoles(Set<String> roles) {
        this.roles = roles;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public Set<String> getAllowClients() {
        return allowClients;
    }

    public void setAllowClients(Set<String> allowClients) {
        this.allowClients = allowClients;
    }
}
