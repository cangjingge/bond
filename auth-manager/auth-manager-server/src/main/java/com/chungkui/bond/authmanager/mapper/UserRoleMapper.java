package com.chungkui.bond.authmanager.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chungkui.bond.authmanager.bean.UserRole;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserRoleMapper extends BaseMapper<UserRole> {
}
