package com.chungkui.bond.authmanager.utils;

import com.chungkui.bond.authmanager.bean.Client;
import com.chungkui.bond.common.core.api.ResBus;
import com.chungkui.bond.common.core.bean.BondLine;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;

import java.util.List;
import java.util.Map;

public class BondUtil {
    private BondUtil() {
    }

    public static ResBus buildG6Data(List<BondLine> bondLineList, List<Client> clientList) {
        List<Map<String, Object>> nodes = Lists.newArrayList();
        List<Map<String, Object>> edges = Lists.newArrayList();
        Map <String, Object>style=Maps.newHashMap();
        style.put("endArrow",true);
        Map<String, Object> data = Maps.newHashMap();
        for (BondLine bondLine : bondLineList) {
            Map<String, Object> line = Maps.newHashMap();
            line.put("source", bondLine.getStartPoint());
            line.put("target", bondLine.getEndPoint());
            line.put("label", "调用");
            line.put("style",style);
            edges.add(line);
        }
        for (Client client : clientList) {
            Map<String, Object> c = Maps.newHashMap();
            c.put("id", client.getClientId());
            c.put("label", StringUtils.isEmpty(client.getClientName())?client.getClientId():client.getClientName());
            nodes.add(c);
        }
        data.put("nodes",nodes);
        data.put("edges",edges);
        return ResBus.success(data);
    }
}
