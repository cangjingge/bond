package com.chungkui.bond.authmanager.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chungkui.bond.common.core.bean.BondLine;
import org.apache.ibatis.annotations.Mapper;

/**
 * 终端配置Mapper接口
 *
 * @author ruoyi
 */
@Mapper
public interface BondLineMapper extends BaseMapper<BondLine>
{

}
