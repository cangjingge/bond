package com.chungkui.bond.authmanager.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.chungkui.bond.authmanager.bean.RoleRouter;

public interface RoleRouterService extends IService<RoleRouter> {

}
