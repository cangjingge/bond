package com.chungkui.bond.authmanager.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.chungkui.bond.authmanager.bean.Client;
import com.chungkui.bond.authmanager.bean.Permission;
import com.chungkui.bond.authmanager.bean.Router;
import com.chungkui.bond.authmanager.bean.UserInfo;
import com.chungkui.bond.authmanager.service.*;
import com.chungkui.bond.authmanager.utils.BondUtil;
import com.chungkui.bond.common.core.api.ResBus;
import com.chungkui.bond.common.core.bean.BondLine;
import com.chungkui.bond.common.core.bean.BondNode;
import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Set;

@RestController
@RequestMapping("/sys")
public class MainController {
    private final static Logger LOG = LoggerFactory.getLogger(MainController.class);

    @Autowired
    private UserInfoService userInfoService;
    @Autowired
    private RouterService routerService;
    @Autowired
    private ClientService clientService;
    @Autowired
    private BondLineService bondLineService;
    @Autowired
    private PermissionService permissionService;
    @GetMapping("/info")
    public Object getInfo(@RequestHeader("client_id") String clientId) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = (User) authentication.getPrincipal();
        Set<String> roles = AuthorityUtils.authorityListToSet(authentication.getAuthorities());
        UserInfo userInfo = userInfoService.getUserByUsername(user.getUsername());
        List<Router> routers = routerService.buildMenuByRoles(roles, clientId);
        Map<String, Object> data = Maps.newHashMap();
        data.put("userinfo", userInfo);
        data.put("routers", routers);
        data.put("roles", roles);
        return ResBus.success(data);
    }



    @PostMapping("/checkClient")
    public ResBus<Boolean> checkClient(@RequestBody Client client) {
        if (clientService.getById(client.getClientId()) != null) {
            return ResBus.failed(false);
        }
        return ResBus.success(true);
    }

    @PostMapping("/registerRelationship")
    public ResBus<Boolean> registerRelationship(@RequestBody BondNode node) {
        clientService.saveBondNodeInfos(node);
        return ResBus.success(true);
    }


    @GetMapping("/queryPermissions")
    public ResBus<List<Permission>> queryPermissions(@RequestHeader("client_id") String clientId) {
        List<Permission> permissions = permissionService.listWithRolesAndClients(clientId);
        return ResBus.success(permissions);
    }
    @GetMapping("/relationship")
    public ResBus<Boolean> relationship() {
        List<BondLine> bondLines = bondLineService.list();
        List<Client> clients = clientService.list();
        return BondUtil.buildG6Data(bondLines, clients);
    }
}
