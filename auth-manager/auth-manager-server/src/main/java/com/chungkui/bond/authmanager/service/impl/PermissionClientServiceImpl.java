package com.chungkui.bond.authmanager.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chungkui.bond.authmanager.bean.PermissionClient;
import com.chungkui.bond.authmanager.mapper.PermissionClientMapper;
import com.chungkui.bond.authmanager.service.PermissionClientService;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PermissionClientServiceImpl extends ServiceImpl<PermissionClientMapper, PermissionClient>
        implements PermissionClientService {

    @Override
    public void saveList(List<PermissionClient> permissionClients) {
        QueryWrapper queryWrapper = new QueryWrapper();
        if (CollectionUtils.isNotEmpty(permissionClients)) {
            for (PermissionClient permissionClient : permissionClients) {
                String id = permissionClient.getPermissionId();
                queryWrapper.eq("permission_id", id);
                break;
            }
            this.baseMapper.delete(queryWrapper);
            this.saveBatch(permissionClients);
        }

    }
}
