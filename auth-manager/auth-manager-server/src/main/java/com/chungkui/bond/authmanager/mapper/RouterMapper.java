package com.chungkui.bond.authmanager.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chungkui.bond.authmanager.bean.Router;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Set;

@Mapper
public interface RouterMapper extends BaseMapper<Router> {
    List<Router>listWithPermission(String clientId) ;
    List<Router>listByRole(Set<String> roles,String clientId);
}
