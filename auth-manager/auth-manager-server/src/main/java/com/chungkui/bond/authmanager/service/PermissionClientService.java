package com.chungkui.bond.authmanager.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.chungkui.bond.authmanager.bean.PermissionClient;
import com.chungkui.bond.authmanager.bean.RouterPermission;

import java.util.List;

public interface PermissionClientService extends IService<PermissionClient> {
   void saveList(List<PermissionClient> permissionClients);
}
