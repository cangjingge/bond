package com.chungkui.bond.authmanager.service.impl;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chungkui.bond.authmanager.bean.Permission;
import com.chungkui.bond.authmanager.mapper.PermissionMapper;
import com.chungkui.bond.authmanager.service.PermissionService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Copyright (C), 2019/5/29, JASON
 * 〈权限信息service〉<br>
 * 〈功能详细描述〉
 *
 * @author jason
 * @fileName: PermissionServiceImpl.java
 * @date: 2019/5/29 20:46
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
@Service
public class PermissionServiceImpl extends ServiceImpl<PermissionMapper, Permission> implements PermissionService {

    @Override
    public List<Permission> listWithRolesAndClients(String clientId) {
        return this.baseMapper.listWithRolesAndClients(clientId);
    }

    @Override
    public void saveBatchByUrlAndClientId(List<Permission> permissions){
          this.baseMapper.saveBatchByUrlAndClientId(permissions);
    }

    @Override
    public IPage<Permission> pageWithClients(Page<Permission> page, String clientId) {
        return  this.baseMapper.pageWithClients(page,clientId);
    }

}
