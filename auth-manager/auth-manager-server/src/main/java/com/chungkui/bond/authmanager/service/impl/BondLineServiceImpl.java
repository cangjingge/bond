package com.chungkui.bond.authmanager.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chungkui.bond.authmanager.mapper.BondLineMapper;
import com.chungkui.bond.authmanager.service.BondLineService;
import com.chungkui.bond.common.core.bean.BondLine;
import org.springframework.stereotype.Service;


@Service
public class BondLineServiceImpl extends ServiceImpl<BondLineMapper, BondLine> implements BondLineService {

}
