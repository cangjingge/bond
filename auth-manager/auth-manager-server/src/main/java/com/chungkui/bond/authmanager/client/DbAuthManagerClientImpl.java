package com.chungkui.bond.authmanager.client;

import com.chungkui.bond.authmanager.bean.Client;
import com.chungkui.bond.authmanager.bean.Permission;
import com.chungkui.bond.authmanager.service.ClientService;
import com.chungkui.bond.authmanager.service.PermissionService;
import com.chungkui.bond.common.core.api.ResBus;
import com.chungkui.bond.common.core.bean.BondNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DbAuthManagerClientImpl implements DbAuthManagerClient{
    @Autowired
    PermissionService permissionService;
    @Autowired
    private ClientService clientService;
    @Override
    public ResBus<Boolean> checkClient(Client client) {
        if (clientService.getById(client.getClientId()) != null) {
            return ResBus.failed(false);
        }
        return ResBus.success(true);
    }

    @Override
    public ResBus<Boolean> registerRelationship(BondNode bondNode) {
        clientService.saveBondNodeInfos(bondNode);
        return ResBus.success(true);
    }

    @Override
    public ResBus<List<Permission>> queryPermissions(String client) {
        List<Permission> permissions = permissionService.listWithRolesAndClients(client);
        return ResBus.success(permissions);
    }
}
