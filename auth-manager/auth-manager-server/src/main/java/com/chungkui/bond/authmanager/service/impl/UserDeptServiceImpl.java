package com.chungkui.bond.authmanager.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chungkui.bond.authmanager.bean.UserDept;
import com.chungkui.bond.authmanager.mapper.UserDeptMapper;
import com.chungkui.bond.authmanager.service.UserDeptService;
import org.springframework.stereotype.Service;

@Service
public class UserDeptServiceImpl extends ServiceImpl<UserDeptMapper, UserDept> implements UserDeptService {

}
