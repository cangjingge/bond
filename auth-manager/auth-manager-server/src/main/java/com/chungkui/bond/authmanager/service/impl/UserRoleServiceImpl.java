package com.chungkui.bond.authmanager.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chungkui.bond.authmanager.bean.UserRole;
import com.chungkui.bond.authmanager.mapper.UserRoleMapper;
import com.chungkui.bond.authmanager.service.UserRoleService;
import com.chungkui.bond.microservice.client.LogoutClient;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements UserRoleService {

    @Autowired
    LogoutClient logoutClient;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean saveUserRoles(String userId, List<UserRole> userRoleList, String optUserId, String userName) {
        Map <String,Object>cmap = new HashMap<>();
        cmap.put("user_id", userId);
        removeByMap(cmap);
        saveBatch(userRoleList);
        if (!StringUtils.equals(userId, optUserId)) {
            logoutClient.logout(userName);
        }
        return true;
    }
}
