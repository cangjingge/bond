package com.chungkui.bond.authmanager.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chungkui.bond.authmanager.bean.RoleRouter;
import com.chungkui.bond.authmanager.mapper.RoleRouterMapper;
import com.chungkui.bond.authmanager.service.RoleRouterService;
import org.springframework.stereotype.Service;

@Service
public class RoleRouterServiceImpl extends ServiceImpl<RoleRouterMapper, RoleRouter> implements RoleRouterService {

}
