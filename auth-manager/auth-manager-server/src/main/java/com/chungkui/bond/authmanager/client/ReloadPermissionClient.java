package com.chungkui.bond.authmanager.client;

import com.chungkui.bond.common.core.api.ResBus;
import feign.RequestLine;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;

import java.net.URI;

@FeignClient(name = "reloadPermissionClient")
public interface ReloadPermissionClient {

    @RequestLine("delete")
    @DeleteMapping
    ResBus<Boolean> reload(URI baseUri, String permissions);
}
