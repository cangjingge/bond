package com.chungkui.bond.authmanager.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chungkui.bond.authmanager.bean.Router;

import java.util.List;
import java.util.Set;

public interface RouterService extends IService<Router> {

    List<Router> listAll2tree(String clientId);

    List<Router> buildMenuByRoles(Set<String> roles, String clientId);


    List<Router> listByRoles2tree(Set<String> roles,String clientId);
}
