package com.chungkui.bond.authmanager.service;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.chungkui.bond.authmanager.bean.Permission;

import java.util.List;

public interface PermissionService extends IService<Permission> {
    List<Permission> listWithRolesAndClients(String clientId);

    void saveBatchByUrlAndClientId(List<Permission> permissions);

    IPage<Permission> pageWithClients(Page<Permission> page, String clientId);

}
