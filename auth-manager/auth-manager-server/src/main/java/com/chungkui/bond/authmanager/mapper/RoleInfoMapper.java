package com.chungkui.bond.authmanager.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chungkui.bond.authmanager.bean.RoleInfo;
import com.chungkui.bond.authmanager.bean.UserInfo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Set;

@Mapper
public interface RoleInfoMapper extends BaseMapper<RoleInfo> {
    Set<String> listRoleCodeByUser(UserInfo userInfo);
    List<String> listRoutersByRole(String roleCode);
    List<String> listPermissionByRole(String roleCode);
}