package com.chungkui.bond.authmanager.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chungkui.bond.authmanager.bean.RouterPermission;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RouterPermissionMapper extends BaseMapper<RouterPermission> {
}
