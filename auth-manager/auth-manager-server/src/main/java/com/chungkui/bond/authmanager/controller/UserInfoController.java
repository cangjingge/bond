package com.chungkui.bond.authmanager.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.chungkui.bond.microservice.resource.util.SecurityUtils;
import com.chungkui.bond.authmanager.bean.UserDept;
import com.chungkui.bond.authmanager.bean.UserInfo;
import com.chungkui.bond.authmanager.bean.UserRole;
import com.chungkui.bond.authmanager.service.RouterService;
import com.chungkui.bond.authmanager.service.UserDeptService;
import com.chungkui.bond.authmanager.service.UserInfoService;
import com.chungkui.bond.authmanager.service.UserRoleService;
import com.chungkui.bond.common.core.bean.SecurityUser;
import com.chungkui.bond.common.core.api.ResBus;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * Copyright (C), 2019/5/29, JASON
 * 〈用户信息管理〉<br>
 * 〈功能详细描述〉
 *
 * @author jason
 * @fileName: UserInfoController.java
 * @date: 2019/5/29 20:45
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
@RestController
@RequestMapping("/userInfo")
public class UserInfoController {
    private final static Logger LOG = LoggerFactory.getLogger(UserInfoController.class);

    @Autowired
    UserInfoService userInfoService;
    @Autowired
    UserRoleService userRoleService;
    @Autowired
    RouterService routerService;
    @Autowired
    UserDeptService userDeptService;


    @RequestMapping("/list")
    public Object list(UserInfo userInfo, Page page) {
        QueryWrapper queryWrapper = new QueryWrapper<UserInfo>();
        if (StringUtils.isEmpty(userInfo.getUsername())) {
            userInfo.setUsername(null);
        }
        if (StringUtils.isEmpty(userInfo.getName())) {
            userInfo.setName(null);
        }
        queryWrapper.setEntity(userInfo);
        return ResBus.success(userInfoService.page(page, queryWrapper));
    }

    @RequestMapping("/save")
    public Object save(String oldLoginName, String userInfo, String userDept) {
        UserInfo obj = JSONObject.parseObject(userInfo, UserInfo.class);
        List<UserDept> list = JSONArray.parseArray(userDept, UserDept.class);
        userInfoService.saveOrUpdateUser(oldLoginName, obj, list);
        LOG.info("save user{}", userInfo);
        return ResBus.success(obj.getId());
    }

    @RequestMapping("/saveUserRole")
    public Object saveUserRoles(String userId, String userRoles) {
        SecurityUser securityUser = SecurityUtils.getLoginUser();
        String optUserId = securityUser.getId();
        List<UserRole> userRoleList = JSONObject.parseArray(userRoles, UserRole.class);
        UserInfo userInfo =userInfoService.getById(userId);
        boolean result = userRoleService.saveUserRoles(userId, userRoleList, optUserId,userInfo.getUsername());
        LOG.info("save user{}{}", userId, userRoleList);
        return ResBus.success(result);
    }

    @RequestMapping("/delete")
    public Object delete(String loginName, String id) {
        return ResBus.success(userInfoService.removeUserInfo(loginName, id));
    }

    @RequestMapping("/listUserRoles")
    public Object listUserRoles(String userId) {
        Map cmap = new HashMap<>();
        cmap.put("user_id", userId);
        return ResBus.success(userRoleService.listByMap(cmap));
    }

    @RequestMapping("/loadUserDept")
    public Object loadUserDept(String userId) {
        QueryWrapper queryWrapper = new QueryWrapper<UserInfo>();
        queryWrapper.eq("user_id", userId);
        List<UserDept> list = userDeptService.list(queryWrapper);
        return ResBus.success(buildUserDept(list));
    }

    private List<Map<String, Object>> buildUserDept(List<UserDept> list) {
        Map<Integer, Map> map = new HashMap();
        if (list != null) {
            for (UserDept userDept : list) {
                if (map.get(userDept.getDeptId()) == null) {
                    //封装dept对象
                    Map<String, Object> dept = new HashMap();
                    dept.put("id", userDept.getDeptId());
                    Set<String> posts = new HashSet();
                    posts.add(userDept.getPostId());
                    dept.put("posts", posts);
                    map.put(userDept.getDeptId(), dept);
                } else {
                    Map<String, Object> dept = map.get(userDept.getDeptId());
                    Set<String> posts = (Set<String>) dept.get("posts");
                    posts.add(userDept.getPostId());
                }
            }
        }
        return new ArrayList(map.values());
    }

    @RequestMapping("/viewMenu")
    public Object viewMenu(String roles,String clientId) {
        List<String> list = JSONObject.parseArray(roles, String.class);
        if (list.isEmpty()) {
            return ResBus.success(new ArrayList<>());
        }
        return ResBus.success(routerService.listByRoles2tree(new HashSet<>(list),clientId));
    }

    /**
     * 校验用户名
     */
    @PostMapping("/checkUsernameUnique")
    @ResponseBody
    public Object checkUsernameUnique(UserInfo userInfo) {
        return ResBus.success(userInfoService.checkUsernameUnique(userInfo));
    }
}
