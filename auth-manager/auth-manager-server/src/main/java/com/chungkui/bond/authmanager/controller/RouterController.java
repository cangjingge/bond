package com.chungkui.bond.authmanager.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.chungkui.bond.authmanager.bean.Permission;
import com.chungkui.bond.authmanager.bean.Router;
import com.chungkui.bond.authmanager.bean.RouterPermission;
import com.chungkui.bond.authmanager.service.PermissionService;
import com.chungkui.bond.authmanager.service.RouterPermissionService;
import com.chungkui.bond.authmanager.service.RouterService;
import com.chungkui.bond.common.core.api.ResBus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Copyright (C), 2019/5/29, JASON
 * 〈路由控制器〉<br>
 * 〈功能详细描述〉
 *
 * @author jason
 * @fileName: RouterController.java
 * @date: 2019/5/29 20:44
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
@RestController
@RequestMapping("/router")
public class RouterController {
    @Autowired
    RouterService routerService;
    @Autowired
    PermissionService permissionService;
    @Autowired
    RouterPermissionService routerPermissionService;

    @RequestMapping("/save")
    public Object save(Router router) {
        router.setTitle(router.getName());
        routerService.save(router);
        return ResBus.success(router.getId());
    }

    @RequestMapping("/update")
    public Object update(Router router) {
        router.setTitle(router.getName());
        return ResBus.success(routerService.saveOrUpdate(router));
    }

    @RequestMapping("/list")
    public Object list(String clientId) {
        return ResBus.success(routerService.listAll2tree(clientId));

    }

    @RequestMapping("/delete")
    public Object delete(String id, String clientId) {
        if (routerService.removeById(id)) {
            Map<String, Object> columnMap = new HashMap<>();
            /*删除子路由*/
            columnMap.put("parentRouter", id);
            permissionService.removeByMap(columnMap);
            return ResBus.success(id);
        }
        return ResBus.failed(id);
    }

    @RequestMapping("/deletePermission")
    public Object deletePermission(RouterPermission routerPermission) {
        QueryWrapper queryWrapper=new QueryWrapper(routerPermission);
        return ResBus.success(routerPermissionService.remove(queryWrapper));
    }

    @RequestMapping("/savePermission")
    public Object savePermission(RouterPermission routerPermission) {
        routerPermissionService.save(routerPermission);
        return ResBus.success(routerPermission);
    }
}
