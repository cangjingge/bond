package com.chungkui.bond.authmanager.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chungkui.bond.authmanager.bean.PermissionClient;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PermissionClientMapper extends BaseMapper<PermissionClient> {
}
