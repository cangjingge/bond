package com.chungkui.bond.authmanager.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.chungkui.bond.authmanager.bean.RolePermission;

public interface RolePermissionService extends IService<RolePermission> {

}
