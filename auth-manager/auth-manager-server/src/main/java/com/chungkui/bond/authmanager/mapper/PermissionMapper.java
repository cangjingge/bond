package com.chungkui.bond.authmanager.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.chungkui.bond.authmanager.bean.Permission;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface PermissionMapper extends BaseMapper<Permission> {
    List<Permission> listWithRolesAndClients(String clientId);

    void saveBatchByUrlAndClientId(List<Permission> permissions);

    IPage<Permission> pageWithClients(Page<Permission> page, String clientId);
}
