package com.chungkui.bond.authmanager.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chungkui.bond.authmanager.bean.UserDept;


public interface UserDeptService extends IService<UserDept> {

}
