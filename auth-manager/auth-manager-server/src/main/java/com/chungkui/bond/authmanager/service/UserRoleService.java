package com.chungkui.bond.authmanager.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chungkui.bond.authmanager.bean.UserRole;

import java.util.List;

public interface UserRoleService extends IService<UserRole> {
    boolean saveUserRoles(String userId, List<UserRole> userRoleList, String optUserId, String userName);

}
