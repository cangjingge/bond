package com.chungkui.bond.authmanager.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chungkui.bond.authmanager.bean.RoleRouter;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RoleRouterMapper extends BaseMapper<RoleRouter> {
}
