package com.chungkui.bond.authmanager;

import com.chungkui.bond.microservice.resource.annotation.EnableMicroservice;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableMicroservice
public class AuthManagerApplication {
    public static void main(String[] args) {

        SpringApplication.run(AuthManagerApplication.class, args);
    }

}

