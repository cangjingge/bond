package com.chungkui.bond.authmanager.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chungkui.bond.authmanager.bean.RolePermission;
import com.chungkui.bond.authmanager.mapper.RolePermissionMapper;
import com.chungkui.bond.authmanager.service.RolePermissionService;
import org.springframework.stereotype.Service;

@Service
public class RolePermissionServiceImpl extends ServiceImpl<RolePermissionMapper, RolePermission> implements RolePermissionService {

}
