package com.chungkui.bond.authmanager.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chungkui.bond.authmanager.bean.RouterPermission;
import com.chungkui.bond.authmanager.mapper.RouterPermissionMapper;
import com.chungkui.bond.authmanager.service.RouterPermissionService;
import org.springframework.stereotype.Service;

@Service
public class RouterPermissionServiceImpl extends ServiceImpl<RouterPermissionMapper, RouterPermission>
        implements RouterPermissionService {

}
