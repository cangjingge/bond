package com.chungkui.bond.authmanager.controller;


import com.chungkui.bond.authmanager.bean.Client;
import com.chungkui.bond.authmanager.service.ClientService;
import com.chungkui.bond.common.core.api.ResBus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/client")
public class CientController {
    @Autowired
    ClientService clientService;

    @RequestMapping("/list")
    public Object list(Client client) {
        return ResBus.success(clientService.list());
    }


}

