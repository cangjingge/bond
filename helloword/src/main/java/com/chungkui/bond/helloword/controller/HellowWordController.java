package com.chungkui.bond.helloword.controller;

import com.chungkui.bond.common.core.api.ResBus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/helloword")
public class HellowWordController {
    @GetMapping("/hello")
    public Object hello() {
        return ResBus.success("Hello I am Jason!");
    }
}
