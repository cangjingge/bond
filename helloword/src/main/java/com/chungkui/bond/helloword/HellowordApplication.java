package com.chungkui.bond.helloword;

import com.chungkui.bond.microservice.resource.annotation.EnableMicroservice;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableMicroservice
public class HellowordApplication {
    public static void main(String[] args) {

        SpringApplication.run(HellowordApplication.class, args);
    }

}

