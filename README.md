spring clioud alibaba微服务架构权限管理平台;
起名叫bond原因是，各个服务之间相互调用。相互关联。羁绊在一起才能对外提供服务。
目前很多开源方案中。权限数据单独放一个资源服务器(微服务)来提供。
但是在认证成功之前该微服务作为一个微服务也需要权限控制。产生权限循环依赖。
看了目前大部分方案是，让这个服务部分接口裸奔。但是个人感觉有安全风险。内部随便一个应用都可以拿该数据了。
我采用的方案是，认证中心只进行认证。授权放在各个微服务上面控制。一方面减轻认证服务器的压力。另一方面避免了不登录即可访问数据的尴尬。
有一个权限信息管理平台，权限变更后通过消息分发到对应的某微服务上面;
整体架构有了,但是目前还是直接查权限平台的数据库的,后面补充分发逻辑。在RBAC基础上加了一个clientid字段
暂且称为RBAC-C架构

项目目录描述
```lua
bond
├── auth-manager             -- 权限管理
     ├── auth-manager-client -- 权限管理客户端，提供相关bean和远程调用权限管理微服务的feign客户端
     ├── auth-manager-server -- 权限管理微服务
└── commons 
     ├── common-core         -- 常用工具类jar
     ├── common-microservice -- bond oauth2微服务依赖公共jar,提供oauth2资源服务器和oauth2客户端支撑,通过注解EnableMicroservice启用
├── gateway-server           -- 网关
└── oauth2-server            -- oauth2认证服务
└── helloword                -- 微服务demo	 
```
启动方式：

1.启动redis

2.刷mysql脚本

3.启动nacos

4.启动GatewayApplication（网关）

5.启动Oauth2ServerApplication（认证中心）

6.启动AuthManagerApplication（权限配置中心）

7.启动bondweb(权限管理客户端前端)（权限配置中心前端 https://gitee.com/cangjingge/bondweb）

前端启动脚本:
npm install
npm run dev