package com.chungkui.bond.microservice.client;

import com.chungkui.bond.authmanager.client.AuthManagerClientProxy;
import com.chungkui.bond.common.core.constant.MicroserviceConstants;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;

/**
 * 1.检测客户端证书信息是否正确
 * 2.推送微服务依赖关系到管理平台
 */
public class CheckClientRunner implements ApplicationRunner {
    @Autowired
    AuthManagerClientProxy authManagerClientProxy;
    @Autowired
    InitRelationship initRelationship;
    @Value("${spring.application.name}")
    private String appname;
    private final static Logger LOG = LoggerFactory.getLogger(CheckClientRunner.class);

    @Override
    public void run(ApplicationArguments args) throws Exception {

        authManagerClientProxy.registerRelationship(initRelationship.getBondNode());

    }
}
