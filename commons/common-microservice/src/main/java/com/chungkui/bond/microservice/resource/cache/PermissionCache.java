package com.chungkui.bond.microservice.resource.cache;

import com.chungkui.bond.authmanager.bean.Permission;

import java.util.List;

public interface PermissionCache {
    void reload();

    List<Permission> getAll();
}
