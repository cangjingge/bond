

package com.chungkui.bond.microservice.client.config;


import com.chungkui.bond.authmanager.client.AuthManagerClientProxy;
import feign.RequestInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.AccessTokenProvider;
import org.springframework.security.oauth2.client.token.AccessTokenProviderChain;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;
import org.springframework.security.oauth2.client.token.grant.code.AuthorizationCodeAccessTokenProvider;
import org.springframework.security.oauth2.client.token.grant.implicit.ImplicitAccessTokenProvider;
import org.springframework.security.oauth2.client.token.grant.password.ResourceOwnerPasswordAccessTokenProvider;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;


@ConditionalOnProperty("security.oauth2.client.client-id")
@Configuration
@EnableFeignClients(basePackages = "com.chungkui.bond")
public class OAuth2FeignConfig {
    @Autowired
    RestTemplate restTemplate;

    @Bean
    @ConfigurationProperties(prefix = "security.oauth2.client")
    public ClientCredentialsResourceDetails clientCredentialsResourceDetails() {
        return new ClientCredentialsResourceDetails();
    }


    OAuth2ClientContext oAuth2ClientContext = new DefaultOAuth2ClientContext();

    @Bean
    public RequestInterceptor requestInterceptor(ClientCredentialsResourceDetails clientCredentialsResourceDetails) {
        ClientCredentialsAccessTokenProvider credentialsAccessTokenProvider = new ClientCredentialsAccessTokenProvider();
        credentialsAccessTokenProvider.setRestTemplate(restTemplate);
        BondOAuth2FeignRequestInterceptor auth2FeignRequestInterceptor = new BondOAuth2FeignRequestInterceptor(oAuth2ClientContext, clientCredentialsResourceDetails);
        auth2FeignRequestInterceptor.setAccessTokenProvider(new AccessTokenProviderChain(
                Arrays.<AccessTokenProvider>asList(new AuthorizationCodeAccessTokenProvider(),
                        new ImplicitAccessTokenProvider(),
                        new ResourceOwnerPasswordAccessTokenProvider(),
                        credentialsAccessTokenProvider)));
        return auth2FeignRequestInterceptor;
    }

    @Bean
    @LoadBalanced
    public RestTemplate clientCredentialsRestTemplate(ClientCredentialsResourceDetails clientCredentialsResourceDetails) {
        return new OAuth2RestTemplate(clientCredentialsResourceDetails);
    }
    @Bean
    AuthManagerClientProxy authManagerClientProxy(){
        return new AuthManagerClientProxy();
    }
}


