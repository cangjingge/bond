package com.chungkui.bond.microservice.resource.config;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.type.AnnotationMetadata;


public class SecurityImportBeanDefinitionRegistrar implements ImportBeanDefinitionRegistrar {
    @Override
    public void registerBeanDefinitions(AnnotationMetadata metadata, BeanDefinitionRegistry registry) {
        Class<BondResourceServerConfigurerAdapter> aClass = BondResourceServerConfigurerAdapter.class;
        String beanName = StringUtils.uncapitalize(aClass.getSimpleName());
        BeanDefinitionBuilder beanDefinitionBuilder = BeanDefinitionBuilder.genericBeanDefinition(BondResourceServerConfigurerAdapter.class);
        registry.registerBeanDefinition(beanName, beanDefinitionBuilder.getBeanDefinition());


        Class<BondGlobalMethodSecurityConfiguration> bondGlobalMethodSecurityConfigurationClass = BondGlobalMethodSecurityConfiguration.class;
        String bondGlobalMethodSecurityConfigurationClassName = StringUtils.uncapitalize(bondGlobalMethodSecurityConfigurationClass.getSimpleName());
        BeanDefinitionBuilder beanDefinition = BeanDefinitionBuilder.genericBeanDefinition(BondGlobalMethodSecurityConfiguration.class);
        registry.registerBeanDefinition(bondGlobalMethodSecurityConfigurationClassName, beanDefinition.getBeanDefinition());


    }
}
