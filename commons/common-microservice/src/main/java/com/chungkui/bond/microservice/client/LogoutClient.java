package com.chungkui.bond.microservice.client;

import com.chungkui.bond.common.core.api.ResBus;
import com.chungkui.bond.common.core.constant.MicroserviceConstants;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Component
@FeignClient(contextId = "logoutClient", value = MicroserviceConstants.AUTH_SERVER)
public interface LogoutClient {

    @PostMapping("/oauth/logoutByUsername")
    ResBus<Boolean> logout(@RequestParam(value ="username") String username);


}
