
package com.chungkui.bond.microservice.resource.config;



import com.alibaba.fastjson.JSONObject;
import com.chungkui.bond.common.core.api.ResBus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@Component
public class BondAccessDeniedHandler extends OAuth2AccessDeniedHandler {

	/**
	 * 授权拒绝处理，使用R包装
	 * @param request request
	 * @param response response
	 * @param authException authException
	 */
	@Override
	public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException authException) throws IOException {
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json");
		ResBus<Exception> result = ResBus.failed(new Exception("授权失败，禁止访问"));
		response.setStatus(403);
		PrintWriter printWriter = response.getWriter();
		printWriter.append(JSONObject.toJSONString(result));
	}

}
