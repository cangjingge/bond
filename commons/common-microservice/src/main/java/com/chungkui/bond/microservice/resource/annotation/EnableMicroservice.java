package com.chungkui.bond.microservice.resource.annotation;


import com.chungkui.bond.microservice.client.config.OAuth2RestTemplateConfig;
import com.chungkui.bond.microservice.resource.config.SecurityImportBeanDefinitionRegistrar;
import com.chungkui.bond.microservice.client.config.OAuth2FeignConfig;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableAsync;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
// 表示通过aop框架暴露该代理对象,AopContext能够访问
@EnableAspectJAutoProxy(exposeProxy = true)
// 指定要扫描的Mapper类的包的路径

// 开启线程异步执行
@EnableAsync
// 自动加载类
@Import({OAuth2RestTemplateConfig.class,SecurityImportBeanDefinitionRegistrar.class, OAuth2FeignConfig.class})
public @interface EnableMicroservice {

}
