package com.chungkui.bond.microservice.resource.config;

import com.chungkui.bond.microservice.resource.methodsecurity.BondMethodSecurityExpressionHandler;
import com.chungkui.bond.microservice.resource.methodsecurity.BondPermissionEvaluator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.oauth2.OAuth2ClientProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.expression.method.MethodSecurityExpressionHandler;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.GlobalMethodSecurityConfiguration;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class BondGlobalMethodSecurityConfiguration extends GlobalMethodSecurityConfiguration {
    @Autowired
    private OAuth2ClientProperties oAuth2ClientProperties;

    @Override
    protected MethodSecurityExpressionHandler createExpressionHandler() {
        BondMethodSecurityExpressionHandler expressionHandler = new BondMethodSecurityExpressionHandler();
        expressionHandler.setPermissionEvaluator(bondPermissionEvaluator());
        return expressionHandler;
    }

    @Bean
    public BondPermissionEvaluator bondPermissionEvaluator() {
        BondPermissionEvaluator bondPermissionEvaluator = new BondPermissionEvaluator();
        bondPermissionEvaluator.setClientId(oAuth2ClientProperties.getClientId());
        return bondPermissionEvaluator;
    }
}
