package com.chungkui.bond.microservice.resource.methodsecurity;

import com.google.common.collect.Sets;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.io.Serializable;
import java.util.Collection;
import java.util.Set;

public class BondPermissionEvaluator implements PermissionEvaluator {

    private String clientId;

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    @Override
    public boolean hasPermission(Authentication authentication, Object target, Object permission) {
        Collection<GrantedAuthority> authorities = (Collection<GrantedAuthority>) authentication.getAuthorities();
        Set<String> roles = Sets.newHashSet();
        if (CollectionUtils.isNotEmpty(authorities)) {
            for (GrantedAuthority authority : authorities) {
                roles.add(authority.getAuthority());
            }
        }
        //todo 暂时不启用方法校验
        return false;
    }

    @Override
    public boolean hasPermission(Authentication authentication, Serializable serializable, String s, Object o) {
        return false;
    }
}
