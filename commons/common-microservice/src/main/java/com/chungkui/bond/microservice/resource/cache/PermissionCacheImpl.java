package com.chungkui.bond.microservice.resource.cache;

import com.chungkui.bond.authmanager.bean.Permission;
import com.chungkui.bond.authmanager.client.AuthManagerClientProxy;
import com.chungkui.bond.authmanager.client.FeignAuthManagerClient;
import com.chungkui.bond.common.core.api.ResBus;
import com.google.common.collect.Lists;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import javax.annotation.PostConstruct;
import java.util.List;

public class PermissionCacheImpl implements PermissionCache {
    private List<Permission> permissions;

    private String clientId;
    @Value("${spring.application.name}")
    private String appname;

    public PermissionCacheImpl(String clientId) {
        this.clientId = clientId;
    }

    @Autowired
    AuthManagerClientProxy authManagerClientProxy;


    @PostConstruct
    public void init() {
        permissions = Lists.newArrayList();
    }

    @Override
    public void reload() {
        ResBus<List<Permission>> data = authManagerClientProxy.queryPermissions(clientId);
        List<Permission> ps = data.getData();
        if (ps != null) {
            synchronized (permissions) {
                permissions = ps;
            }
        }
    }

    /**
     * @return
     */
    @Override
    public List<Permission> getAll() {
        //todo 权限为空的时候可能有击穿问题
        if (CollectionUtils.isEmpty(permissions)) {
            reload();
        }
        return permissions;
    }
}
