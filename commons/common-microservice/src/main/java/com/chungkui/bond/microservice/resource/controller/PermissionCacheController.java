package com.chungkui.bond.microservice.resource.controller;

import com.chungkui.bond.common.core.api.ResBus;
import com.chungkui.bond.microservice.resource.cache.PermissionCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PermissionCacheController {
    @Autowired
    PermissionCache permissionCache;
    @DeleteMapping("/sys/reloadPermission")
    public ResBus<Boolean> reload() {
        permissionCache.reload();
        return ResBus.success();
    }
}
