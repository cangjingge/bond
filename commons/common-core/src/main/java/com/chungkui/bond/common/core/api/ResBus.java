
package com.chungkui.bond.common.core.api;


import java.io.Serializable;



public class ResBus<T> implements Serializable {
    private static final long serialVersionUID = 1L;
    private int code;
    private String msg;
    private T data;

    public static <T> ResBus<T> success() {
        return restResult(null, 200, null);
    }

    public static <T> ResBus<T> success(T data) {
        return restResult(data, 200, null);
    }

    public static <T> ResBus<T> success(T data, String msg) {
        return restResult(data, 200, msg);
    }

    public static <T> ResBus<T> failed() {
        return restResult(null, 500, null);
    }

    public static <T> ResBus<T> failed(String msg) {
        return restResult(null, 500, msg);
    }

    public static <T> ResBus<T> failed(T data) {
        return restResult(data, 500, null);
    }

    public static <T> ResBus<T> failed(T data, String msg) {
        return restResult(data, 500, msg);
    }

    public static <T> ResBus<T> failed(int code, String msg) {
        return restResult(null, code, msg);
    }

    public static <T> ResBus<T> restResult(T data, int code, String msg) {
        ResBus<T> apiResult = new ResBus<>();
        apiResult.setCode(code);
        apiResult.setData(data);
        apiResult.setMsg(msg);
        return apiResult;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ResBus{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                '}';
    }
}
