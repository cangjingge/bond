package com.chungkui.bond.common.core.utils;

import feign.Target;

import java.lang.reflect.Field;

public class FeignClientParseUtil {
    private FeignClientParseUtil() {
    }


    public static String getName(Object proxy) throws Exception {
        Field h = proxy.getClass().getSuperclass().getDeclaredField("h");
        h.setAccessible(true);
        Object obj=h.get(proxy);
        Field target=obj.getClass().getDeclaredField("target");
        target.setAccessible(true);
        Target res= (Target) target.get(obj);
        return res.name();
    }
}
