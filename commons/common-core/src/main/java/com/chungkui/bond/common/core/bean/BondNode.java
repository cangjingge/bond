package com.chungkui.bond.common.core.bean;

import java.util.List;
import java.util.Map;

public class BondNode {
    private String id;
    private String name;
    private List<BondLine> bondLines;
    private List<Map<String,String>> urls;
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<BondLine> getBondLines() {
        return bondLines;
    }

    public void setBondLines(List<BondLine> bondLines) {
        this.bondLines = bondLines;
    }

    public List<Map<String, String>> getUrls() {
        return urls;
    }

    public void setUrls(List<Map<String, String>> urls) {
        this.urls = urls;
    }
}
